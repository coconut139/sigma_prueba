<?php

    include_once('db/conexionSigma.php');


    $conexionSigma = new conexionSigma();
    $conexion = $conexionSigma->conectar();

    $state = trim($_POST['departamento']);
    $city = trim($_POST['ciudad']);
    $name = trim(mb_strtoupper($_POST['nombre']));
    $email = trim(mb_strtolower($_POST['correo']));

    $response = array();

    try {
        $conexion->beginTransaction();

        $sql = $conexion->prepare("
            insert into contacts(name, email, state, city)
            values(:name, :email, :state, :city)
        ");
        $sql->bindParam(':name',$name);
        $sql->bindParam(':email',$email);
        $sql->bindParam(':state', $state);
        $sql->bindParam(':city',$city);
        $sql->execute();

        $conexion->commit();
        
        $response['estado'] = "ok";
        $response['mensaje'] = "Tu información ha sido recibida satisfactoriamente.";

    } catch (\PDOException $e) {
        $conexion->rollBack();
        $response['estado'] = "error";
        $response['mensaje'] = "Ocurrio un problema al recibir tu información. Error: $e";
    }

    echo json_encode($response);
    exit();

